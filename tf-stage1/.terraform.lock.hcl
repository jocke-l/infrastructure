# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/external" {
  version = "2.2.2"
  hashes = [
    "h1:e7RpnZ2PbJEEPnfsg7V0FNwbfSk0/Z3FdrLsXINBmDY=",
    "zh:0b84ab0af2e28606e9c0c1289343949339221c3ab126616b831ddb5aaef5f5ca",
    "zh:10cf5c9b9524ca2e4302bf02368dc6aac29fb50aeaa6f7758cce9aa36ae87a28",
    "zh:56a016ee871c8501acb3f2ee3b51592ad7c3871a1757b098838349b17762ba6b",
    "zh:719d6ef39c50e4cffc67aa67d74d195adaf42afcf62beab132dafdb500347d39",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:7fbfc4d37435ac2f717b0316f872f558f608596b389b895fcb549f118462d327",
    "zh:8ac71408204db606ce63fe8f9aeaf1ddc7751d57d586ec421e62d440c402e955",
    "zh:a4cacdb06f114454b6ed0033add28006afa3f65a0ea7a43befe45fc82e6809fb",
    "zh:bb5ce3132b52ae32b6cc005bc9f7627b95259b9ffe556de4dad60d47d47f21f0",
    "zh:bb60d2976f125ffd232a7ccb4b3f81e7109578b23c9c6179f13a11d125dca82a",
    "zh:f9540ecd2e056d6e71b9ea5f5a5cf8f63dd5c25394b9db831083a9d4ea99b372",
    "zh:ffd998b55b8a64d4335a090b6956b4bf8855b290f7554dd38db3302de9c41809",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version = "1.34.3"
  hashes = [
    "h1:ZAFEiotL15xHI957CrTJEm3z83/lFMQdNWiMqN7Dyu4=",
    "zh:08e4aea3c4e895d941c24d76f373977e5fae112c531b56cb2fcdaa8b5086ebec",
    "zh:09f1255aad8f790c86a14522c7fff2fcb6529bb18f1e49a7423ca79db07f5a47",
    "zh:0b2bcab12aa913adaf89228a250eb41e827c64723a741f2b74b318833f77c9df",
    "zh:0c6947f73dc5779b2425e231b22643741b1877705fb4135df06f7a201f10d094",
    "zh:26513bdefecae83411997a5c4e11dffd886b4594cb8663f4b0c36a064b1a650e",
    "zh:42790576142fb7d0ce851923f58a642e7e4c9e6df9a02b531703131d836daade",
    "zh:5095ea6c8b31dfebd52a03f9ba4e2a8a7ecb0c0442f68c98cab76b677c343e81",
    "zh:6b80223b2831f9f2adf72c7ed37692cef67c8f43de62c405c7162bf524896bc3",
    "zh:6b90164e839ad0977b3feb4dbec153ab76a4f7ee8451ee2d202992ff5a4157b1",
    "zh:72d88705afb9d0a9d90a5ad342c605cb119b678e76f45c5e4c516da5d591f907",
    "zh:881889a9e974a15c616cd7648f1c415a4fb3e9a08c166bcb17e488bfcbb67985",
    "zh:967286f7ef73bb15c2412b9ae8853d1176a039374c5ea39cbfd75e7ebe13bcfd",
    "zh:967a6cc506a019c1720f0207068097a06eb006e940ecc63060373b4b4b6181cc",
    "zh:a68e5e2e2f03656e427045e32ca999422e00be299fc6ecedbd41bef48871b7c5",
  ]
}

provider "registry.terraform.io/timohirt/hetznerdns" {
  version = "2.1.0"
  hashes = [
    "h1:fNDrNbEUkv/TnUdg5rqHhDyI9hp3ScJ0tW/psaKfHY4=",
    "zh:1b1e1245a3c57995cddd0e094411715d9a71043ead2d4f427b5bd364616b5822",
    "zh:4b3f4583cad7a106e8dad0252946df34ae908fd7237b1744b500a0428f3e6b02",
    "zh:508bdd2860a2ae3168093904bdb3a654ed6ba1a9036207a4c154cf22fd2ab4a5",
    "zh:8f9bf0ff1d4f28e996e28c7c3de42a2c7487afdbba89033e22f51759dfe3f429",
    "zh:a2708d50f5714e09b252010e6f1c684d92d0935df609be839c02b9eb4ce661d2",
    "zh:ab338f412a020f57931b4852c0ab236a592756d7c3f4a4adb3fe7b96bd847d27",
    "zh:c5491e35d3ddd777eab87b6c1b0ac7890e77eb611b8b1dcf6132af665f3a57d8",
    "zh:daffa4c068ceb6d6b4af87cdb3c590ddd2047c3f9e6bc8048a414ea7d99942b0",
  ]
}
